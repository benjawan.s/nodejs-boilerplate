import { getServerHost, getServerPort } from '../helpers';
import { expect } from 'chai';

describe('helpers', function () {
  it('should output the server hostname', function (done) {
    const serverHost = getServerHost({
      headers: {
        host: 'localhost:3000',
      },
    });
    expect(serverHost).to.equal('localhost');
    done();
  });

  it('should output the server port', function (done) {
    const serverPort = getServerPort({
      headers: {
        host: 'localhost:3000',
      },
    });
    expect(serverPort).to.equal('3000');
    done();
  });
});
