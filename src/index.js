import http from 'http';
import {} from 'dotenv/config';

const server = http.createServer((req, res) => {
  res.writeHead(200, 'OK', { 'Content-Type': 'text/plain' });
  res.end(`Hello world!`);
});

server.listen(process.env.SERVER_PORT, () => {
  console.log(`Server is listening on port ${process.env.SERVER_PORT}`);
});
