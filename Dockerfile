FROM node:14.16.0-alpine as builder

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

FROM node:14.16.0-alpine

RUN npm i -g nodemon
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY --from=builder /usr/src/app/dist ./dist
ENV SERVER_PORT=3000
EXPOSE 3000
CMD ["npm", "run", "prod"]
